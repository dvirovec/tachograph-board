from __future__ import unicode_literals

from django.db import models
from datetime import datetime


class Company(models.Model):
    companyName = models.CharField(max_length=100)
    dateCreated = models.DateTimeField(auto_now_add=True)
    dataFilePath = models.CharField(max_length=512, default="")

    def __unicode__(self):
        return self.companyName


class Log(models.Model):

    levels = ['All','Critical', 'Error', 'Warning', 'Information', 'Verbose']

    logTime = models.DateTimeField(null=False, default=datetime.now)
    company = models.CharField(max_length=50)
    logLevel = models.IntegerField(null=False)
    logText = models.CharField(max_length=1000)

    def __unicode__(self):
        return "{} {}:{}".format(str(self.logTime), self.levels[self.logLevel], self.logText)


