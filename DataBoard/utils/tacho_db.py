import psycopg2
import sys
from xml.dom import minidom
from django.conf import settings
from django.http import Http404
import os
import time


def write_records(records, company_name):

    if not records:
        return 'NOK'

    conn = get_conn(company_name=company_name)

    if conn:
        for rec in records:
            cur = conn.cursor()

            command = "INSERT INTO history " \
                      "( Id, Timestamp, Number, Files, IMEI, TachoTimeFrom, TachoTimeTo, DataBlocks, Company ) " \
                      "VALUES ('{0}',{1},'{2}','{3}','{4}',{5},{6},'{7}','{8}')".format(rec["Id"],
                                '\'' + rec["Timestamp"] + '\'' if rec["Timestamp"] else 'null',
                                rec["Number"], rec["Files"].strip(), rec["IMEI"],
                                '\'' + rec["TachoTimeFrom"] + '\'' if rec["TachoTimeFrom"] else 'null',
                                '\'' + rec["TachoTimeTo"] + '\'' if rec["TachoTimeTo"] else 'null',
                                rec["DataBlocks"], company_name.lower())

            cur.execute(command)

            last_timestamp = rec["Timestamp"]
            cur.execute("UPDATE status SET  last_timestamp = '{0}';".format(last_timestamp))

            cur.close()

            conn.commit()

        return 'OK'

    return 'NOK'


def read_all_records(company_name):
    return filter_records(imei=None, ts_from=None, ts_to=None, tm_from=None, tm_to=None, company_name=company_name)


def filter_records(imei, ts_from, ts_to, tm_from, tm_to, company_name):

    date_format = 'YYYY-MM-dd HH:mm:ss'

    results = []

    conn = get_conn(company_name="")

    if conn:
        cur = conn.cursor()
    else:
        return results

    select_sql = "SELECT CAST(Id AS VARCHAR(50)) Id, to_char(Timestamp,'{0}') as Timestamp, " \
                 "Number, Files, IMEI, to_char(TachoTimeFrom, '{0}') as TachoTimeFrom, " \
                 " to_char(TachoTimeTo,'{0}') as TachoTimeTo , DataBlocks ".format(date_format)

    from_sql =  " FROM History "
    where_sql = " WHERE 1=1 " if company_name == "*" else " WHERE Company = '{}'".format(company_name.lower())

    order_sql = " ORDER BY Timestamp DESC"

    # if imei or ts_from or ts_to or tm_from or tm_to:
    #     where_sql += " WHERE "

    where_sql_len = len(where_sql)

    if imei:
        where_sql += " imei LIKE '{}%' ".format(imei)

    if ts_from:
        ts_from = time.strptime(ts_from, "%d.%m.%Y")
        ts_from = time.strftime("%Y-%m-%d %H:%M:%S", ts_from)

    if ts_to:
        ts_to += " 23:59:59"
        ts_to = time.strptime(ts_to, "%d.%m.%Y %H:%M:%S")
        ts_to = time.strftime("%Y-%m-%d %H:%M:%S", ts_to)

    if ts_from and ts_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                    " Timestamp >= '{0}' AND Timestamp < '{1}'".format(ts_from, ts_to)

    if ts_from and not ts_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                     " Timestamp >= '{0}'".format(ts_from)

    if not ts_from and ts_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                     " Timestamp <= '{0}'".format(ts_to)

    if tm_from:
        tm_from = time.strptime(tm_from, "%d.%m.%Y")
        tm_from = time.strftime("%Y-%m-%d %H:%M:%S", tm_from)

    if tm_to:
        tm_to += " 23:59:59"
        tm_to = time.strptime(tm_to, "%d.%m.%Y %H:%M:%S")
        tm_to = time.strftime("%Y-%m-%d %H:%M:%S", tm_to)

    if tm_from and tm_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                     " TachoTimeFrom >= '{0}' AND TachoTimeTo < '{1}'".format(tm_from, tm_to)

    if tm_from and not tm_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                     " TachoTimeFrom >= '{0}'".format(tm_from)

    if not tm_from and tm_to:
        where_sql += " AND " if len(where_sql) > where_sql_len else "" + \
                     " TachoTimeTo <= '{0}'".format(tm_to)

    command = select_sql+from_sql+where_sql+order_sql

    columns = ('Id', 'Timestamp', 'Number', 'Files', 'IMEI', 'TachoTimeFrom', 'TachoTimeTo', 'DataBlocks', 'Company')

    cur.execute(command)

    for row in cur.fetchall():
        results.append(dict(zip(columns, row)))

    return results


def get_last_timestamp(company_name):

    conn = get_conn(company_name="")

    last_timestamp = None

    if conn:
        cur = conn.cursor()
        cur.execute("SELECT last_timestamp FROM status;")
        row = cur.fetchone()

        if not row:
            last_timestamp = '2000-01-01 00:00:00'
            cur.execute("INSERT INTO status ( last_timestamp) VALUES ('{0}');".format(last_timestamp))
            conn.commit()
        else:
            last_timestamp = row[0]

    return last_timestamp


def get_conn(company_name):
    return psycopg2.connect(database="TachoDb", user="postgres", password="postgres", host="192.168.226.199", port="5432")

    if company_name == "":
        db_name = "TachoDb"
    else:
        db_name = "Tacho{}".format(company_name)

    try:
        if not settings.__getattr__("DB_NAME") or settings.__setattr__("DB_NAME", db_name) != db_name:
            settings.__setattr__("DB_NAME", db_name)
            settings.__setattr__("DB_CONN", create_conn(db_name=db_name))
    except Exception:
            e = sys.exc_info()[0]
            print(e)

    return settings.__getattr__("DB_CONN")


def create_conn(db_name):
    return psycopg2.connect(database=db_name, user="postgres", password="postgres", host="127.0.0.1", port="5432")


def get_config():

    if not settings.__getattr__("CONFIG_FILE"):
        raise Http404("CONFIG_FILE configuration parameter does not exists.")

    config_file_name = "{}\{}".format(settings.__getattr__("ROOT_PATH"), settings.__getattr__("CONFIG_FILE"))

    if not os.path.exists(config_file_name):
        raise Http404("Configuration file {} does not exists.".format(config_file_name))

    config = minidom.parse(config_file_name)

    process_list = config.getElementsByTagName('Process')

    comp_list = []

    for p in process_list:
        comp_list.append({'CompanyName': p.attributes['CompanyName'].value, 'DataFilesLocation': p.attributes['DataFilesLocation'].value})

    return comp_list
