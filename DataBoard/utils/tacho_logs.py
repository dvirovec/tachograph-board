import os
from DataBoard.models import Log
import tacho_db
import time


def get_log(request, root_path, selected_company, log_level):

    if selected_company == 'service':
        log_dir = root_path + '\\Log'
    else:
        log_dir = root_path + '\\' + selected_company + '\\Log'

    log_file_name = ''

    log_content = ''

    conn = tacho_db.get_conn(selected_company)

    cur = conn.cursor()

    query_list = ['SELECT "logTime", "logLevel", "logText" FROM "DataBoard_log" '
                  'WHERE "company" = @{}@'.format(selected_company)]

    if log_level > 0:
        query_list.append(' AND "logLevel" <= {}'.format(log_level))

    query_list.append(' ORDER BY "logTime" DESC;')

    query_string = ''.join(query_list)

    query_string = query_string.replace('@', '\'')

    cur.execute(query_string)

    logs = []

    for log in cur.fetchall():
        level = 4 if log[1] == 9 else log[1]
        logs.append({'logTime': log[0].strftime('%d.%m.%Y %H.%M.%S'), 'logLevel': Log.levels[level], 'logText': log[2]})

    return logs, log_file_name, log_content
