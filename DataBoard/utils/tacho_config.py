from xml.dom import minidom
from django.conf import settings
from django.http import Http404
import os


def get_config():

    if not settings.__getattr__("CONFIG_FILE"):
        raise Http404("CONFIG_FILE configuration parameter does not exists.")

    config_file_name = "{}\{}".format(settings.__getattr__("ROOT_PATH"), settings.__getattr__("CONFIG_FILE"))

    if not os.path.exists(config_file_name):
        raise Http404("Configuration file {} does not exists.".format(config_file_name))

    config = minidom.parse(config_file_name)

    process_list = config.getElementsByTagName('Process')

    comp_list = []

    for p in process_list:
        comp_list.append({'CompanyName': p.attributes['CompanyName'].value, 'DataFilesLocation': p.attributes['DataFilesLocation'].value,
                          'company_code': p.attributes['company_code'].value})

    return comp_list
