from django.conf.urls import url
from . import views

app_name = 'board'

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^save_records$', views.post_records, name='postRecords'),
    url(r'^login$', views.do_login, name='do_login'),
    url(r'^logout$', views.do_logout, name='do_logout'),
    url(r'^config$', views.config, name='config'),
    url(r'^timestamp$', views.last_timestamp, name='last_timestamp'),
    url(r'^datafile/(?P<filename>[\w\d\.\ ]+)/$', views.get_datafile, name='datafile'),
    url(r'^log_line$', views.log_line_db, name='log_line'),
    url(r'^notification$', views.notification, name='notification'),
    url(r'^get_log_list$', views.get_log_list, name='get_log_list'),
    url(r'^load_ddd_file$', views.load_ddd_file, name='load_ddd_file')

]
