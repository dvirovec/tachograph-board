from __future__ import print_function
import os
import json
import urllib
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import redirect
from django.shortcuts import render
from django.contrib.auth import authenticate, login, logout
from django.conf import settings
from django.http import Http404
import mimetypes
from utils import tacho_config
from utils import tacho_db
from utils import tacho_logs
from models import Log
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile


def index(request):

    if not request.user.is_authenticated():
        return redirect('board:do_login')

    if request.session.get("context"):
        context = request.session["context"]
    else:
        context = {}

    if not context.get("log_levels"):
        context["log_levels"] = Log.levels

    if not context.get('selected_log_level'):
        context['selected_log_level'] = 'Information'
        context['selected_log_level_index'] = Log.levels.index('Information')

    if request.POST.get('selected_log_level'):
        context['selected_log_level'] = request.POST.get('selected_log_level')
        context['selected_log_level_index'] = Log.levels.index(request.POST.get('selected_log_level'))

    context['company_list'] = tacho_config.get_config()

    if not settings.__getattr__('ROOT_PATH'):
        raise Http404("ROOT_PATH configuration parameter does not exists.")

    root_path = settings.__getattr__('ROOT_PATH')

    if request.POST.get('display_mode'):
        context['display_mode'] = request.POST.get('display_mode')
    else:
        if not context.get('display_mode'):
            context['display_mode'] = "LOG"

    if not context.get('selected_company'):
        context['selected_company'] = 'service'
        context['selected_company_name'] = "Tacho service"

    if not context.get('user_company'):
        context['user_company'] = '*'

    if request.user.is_authenticated:

        groups = request.user.groups.all()

        if len(groups) > 0:
            context['user_company'] = request.user.groups.all()[0].name
        else:
            context['user_company'] = "*"

        for company in context['company_list']:
            if company['company_code'] == request.POST.get('selected_company') or \
                            company['company_code'] == context['user_company']:

                context['selected_company_name'] = company['CompanyName']
                context['selected_company'] = company['company_code']
                context['company_code'] = company['company_code']
                context['data_file_path'] = company['DataFilesLocation']
                break

        if context['user_company'] == "*":
            if request.POST.get('selected_company') == "service":
                context['company_code'] = 'service'
                context['selected_company'] = 'service'
                context['selected_company_name'] = 'Tacho service'
                context['data_file_path'] = ""

    context['logs'], context['log_file_name'], context['log_content'] \
        = tacho_logs.get_log(request=request, root_path=root_path, selected_company=context['selected_company'],
                             log_level=context['selected_log_level_index'])

    if context.get('vehicleData'):
        del context['vehicleData']

    if context['display_mode'] == 'DATA' and context['user_company'] == "*":
        if request.POST.get("search"):
            json_data = tacho_db.filter_records(request.POST.get("imei"), request.POST.get("ts_from"),
                                                request.POST.get("ts_to"), request.POST.get("tm_from"),
                                                request.POST.get("tm_to"), context.get('selected_company'))
        else:
            json_data = tacho_db.read_all_records(company_name=context['selected_company'])

        context['vehicleData'] = json_data

    request.session['context'] = context

    return render(request=request, template_name='DataBoard/index.html', context=request.session['context'])


def do_login(request):
    if request.POST.get('username'):
        user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
        if user:
            if user.is_active:
                login(request, user)
                return redirect('board:index')

    return render(request, 'DataBoard/login.html')


def do_logout(request):
    logout(request)
    return redirect('board:index')


@csrf_exempt
def config(request):

    if request.POST.get('config'):
        config_data = request.POST.get('config')
        json_data = json.loads(config_data)
        settings.__setattr__("DB_NAME", "Tacho"+json_data["CompanyName"])

    return HttpResponse('exit')


@csrf_exempt
def last_timestamp(request):
    if request.POST.get('company'):
        return HttpResponse(tacho_db.get_last_timestamp(request.POST.get('company')))


@csrf_exempt
def post_records(request):

    if request.POST.get('record'):
        record = request.POST.get('record')
        json_data = json.loads(record)
        tacho_db.write_records(json_data, request.POST.get('company'))

    request.POST = None
    return HttpResponse('exit')


def get_datafile(request, filename):

    file_path = '{}\{}'.format(request.session['context']['data_file_path'], filename)

    original_filename = filename

    fp = open(file_path, 'rb')
    response = HttpResponse(fp.read())
    fp.close()

    m_type, encoding = mimetypes.guess_type(original_filename)

    if m_type is None:
        m_type = 'application/octet-stream'

    if encoding is not None:
        response['Content-Encoding'] = encoding

    response['Content-Type'] = m_type
    response['Content-Length'] = str(os.stat(file_path).st_size)

    if u'WebKit' in request.META['HTTP_USER_AGENT']:
        # Safari 3.0 and Chrome 2.0 accepts UTF-8 encoded string directly.
        filename_header = 'filename=%s' % original_filename.encode('utf-8')
    elif u'MSIE' in request.META['HTTP_USER_AGENT']:
        # IE does not support internationalized filename at all.
        # It can only recognize internationalized URL, so we do the trick via routing rules.
        filename_header = ''
    else:
        # For others like Firefox, we follow RFC2231 (encoding extension in HTTP headers).
        filename_header = 'filename*=UTF-8\'\'%s' % urllib.quote(original_filename.encode('utf-8'))
    response['Content-Disposition'] = 'attachment; ' + filename_header

    return response

@csrf_exempt
def notification(request):

    if request.POST.get('message_text'):

        company_code = request.POST.get('company_code')

        conn = tacho_db.get_conn(company_name="")
        command = 'INSERT INTO "DataBoard_log" ("logLevel", "logText", "logTime", "company")' \
                  ' VALUES ({0}, \'{1}\', NOW(), \'{2}\')'.format(9, request.POST.get('message_text'), company_code)
        cur = conn.cursor()

        cur.execute(command)

        cur.close()

        conn.commit()

    return HttpResponse('OK')


@csrf_exempt
def log_line_db(request):

    if request.POST.get('logLevel'):

        print(request.POST.get('logLevel'))

        company_code = request.POST.get('company_code')

        conn = tacho_db.get_conn(company_name="")

        command = 'INSERT INTO "DataBoard_log" ("logLevel", "logText",' \
                  ' "logTime", "company") ' \
                  'VALUES ({0}, \'{1}\', NOW(), \'{2}\')'.format(request.POST.get('logLevel'),
                                                                 request.POST.get('logText'), company_code)

        cur = conn.cursor()

        cur.execute(command)

        cur.close()

        conn.commit()

    return HttpResponse('OK')


def get_log_list(request):
    context = request.session["context"]
    data = Log.objects.filter(company=context['company_code'])
    return HttpResponse(request, data)


@csrf_exempt
def load_ddd_file(request):

    try:
        if request.FILES:
            ddd_file = request.FILES.get('file')
            file_dir = '{}{}'.format(settings.__getattr__('MEDIA_ROOT'), ddd_file.name[2:10])

            if not os.path.exists(file_dir):
                os.makedirs(file_dir)

            file_path = '{}/{}'.format(file_dir, ddd_file.name)

            default_storage.save(file_path, ContentFile(ddd_file.read()))
    except:
        return HttpResponse('NOK')

    return HttpResponse('OK')
